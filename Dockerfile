FROM ubuntu:xenial-20191108

MAINTAINER Mike Cooper <mikejcooper90@gmail.com>

#Define input/output directories
VOLUME "/input/"
VOLUME "/output"
VOLUME "/model"

#Add and set entrypoint
ADD run.sh ./run.sh
RUN chmod u+x ./run.sh
ENTRYPOINT ./run.sh

#######################
# Customization start #
#######################

#Add any custom dependencies and/or scripts here
RUN apt-get update && \
apt-get install -y python3-pip git apt-utils
RUN python3 -m pip install --upgrade nltk
RUN pip3 install torch==1.3.1+cpu torchvision==0.4.2+cpu -f https://download.pytorch.org/whl/torch_stable.html
RUN pip3 install gensim numpy pyonmttok configargparse tqdm --ignore-installed nltk
RUN pip3 install torchtext --no-deps
RUN git clone https://gitlab.com/mgnc2867/clinicalnmt
RUN cd clinicalnmt
RUN git clone https://github.com/OpenNMT/OpenNMT-py
#RUN python3 OpenNMT-py/setup.py install


COPY /combiNMT995_step_20472.pt /model
#COPY /OpenNMT-py /OpenNMT-py/
COPY /NOTEEVENTS-00161-0.txt /input
COPY /NOTEEVENTS-00339-0.txt /input
COPY /NOTEEVENTS-00652-0.txt /input



 
#######################
# Customization end   #
#######################

